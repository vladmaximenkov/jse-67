package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Project;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectCollectionResource {

    @NotNull
    @GetMapping
    Collection<Project> get();

    @PostMapping
    void post(@NotNull @WebParam(name = "projects") @RequestBody List<Project> projects);

    @PutMapping
    void put(@NotNull @WebParam(name = "projects") @RequestBody List<Project> projects);

    @DeleteMapping
    void delete();

}
