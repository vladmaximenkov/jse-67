package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Project;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/project")
public interface IProjectResource {

    @GetMapping("/{id}")
    Project get(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

    @PostMapping
    void post(@NotNull @WebParam(name = "project") @RequestBody Project project);

    @PutMapping
    void put(@NotNull @WebParam(name = "project") @RequestBody Project project);

    @DeleteMapping("/{id}")
    void delete(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

}
