package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Task;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/task")
public interface ITaskResource {

    @GetMapping("/{id}")
    Task get(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

    @PostMapping
    void post(@NotNull @WebParam(name = "task") @RequestBody Task task);

    @PutMapping
    void put(@NotNull @WebParam(name = "task") @RequestBody Task task);

    @DeleteMapping("/{id}")
    void delete(@NotNull @WebParam(name = "id") @PathVariable("id") String id);

}
