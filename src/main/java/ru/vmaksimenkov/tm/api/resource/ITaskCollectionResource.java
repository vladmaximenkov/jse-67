package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Task;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskCollectionResource {

    @NotNull
    @GetMapping
    Collection<Task> get();

    @PostMapping
    void post(@NotNull @WebParam(name = "tasks") @RequestBody List<Task> tasks);

    @PutMapping
    void put(@NotNull @WebParam(name = "tasks") @RequestBody List<Task> tasks);

    @DeleteMapping
    void delete();

}
