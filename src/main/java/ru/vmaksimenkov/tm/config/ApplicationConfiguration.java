package ru.vmaksimenkov.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.vmaksimenkov.tm")
public class ApplicationConfiguration {
}
