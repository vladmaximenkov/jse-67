package ru.vmaksimenkov.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource;
import ru.vmaksimenkov.tm.api.resource.IProjectResource;
import ru.vmaksimenkov.tm.api.resource.ITaskCollectionResource;
import ru.vmaksimenkov.tm.api.resource.ITaskResource;
import ru.vmaksimenkov.tm.endpoint.ProjectEndpoint;
import ru.vmaksimenkov.tm.endpoint.TaskEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@ComponentScan("ru.vmaksimenkov.tm")
public class WebApplicationConfiguration implements WebMvcConfigurer, WebApplicationInitializer {

    @Bean
    public SpringBus cxf() { return new SpringBus(); }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Override
    public void addViewControllers(@NotNull final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
    }

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final IProjectResource projectEndpoint, SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final ITaskResource taskEndpoint, SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskCollectionEndpointRegistry(@NotNull final ITaskCollectionResource taskCollectionEndpoint, SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskCollectionEndpoint);
        endpoint.publish("/TaskCollectionEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectCollectionEndpointRegistry(@NotNull final IProjectCollectionResource projectCollectionEndpoint, SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectCollectionEndpoint);
        endpoint.publish("/ProjectCollectionEndpoint");
        return endpoint;
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext) throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamicCXF =
                servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}

