package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    private final ProjectRepository repository;

    @Autowired
    public ProjectService(@NotNull final ProjectRepository repository) {
        this.repository = repository;
    }

    @NotNull
    public Collection<Project> findAll() {
        return repository.findAll();
    }

    public void merge(@NotNull final List<Project> list) {
        list.forEach(repository::save);
    }

    public void merge(@NotNull final Project project) {
        repository.save(project);
    }

    public void removeAll() {
        repository.deleteAll();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

}
