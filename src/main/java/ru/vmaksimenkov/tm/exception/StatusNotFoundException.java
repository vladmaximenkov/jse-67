package ru.vmaksimenkov.tm.exception;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error! Status not found...");
    }

}
