package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.ITaskResource;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.service.TaskService;

import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping(value = "/api/task", produces = MediaType.APPLICATION_JSON_VALUE)
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.ITaskResource")
public class TaskEndpoint implements ITaskResource {

    @NotNull
    private final TaskService service;

    @Autowired
    public TaskEndpoint(@NotNull final TaskService service) {
        this.service = service;
    }

    @Nullable
    @Override
    @GetMapping("/{id}")
    public Task get(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        return service.findById(id);
    }

    @Override
    @PostMapping
    public void post(@NotNull @WebParam(name = "task") @RequestBody final Task task) {
        service.merge(task);
    }

    @Override
    @PutMapping
    public void put(@NotNull @WebParam(name = "task") @RequestBody final Task task) {
        service.merge(task);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        service.removeById(id);
    }

}
