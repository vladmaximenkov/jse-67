import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.client.ProjectClient;
import ru.vmaksimenkov.tm.client.ProjectClientFeign;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.*;

public class ProjectTest {

    @Test
    @Category(ApiCategory.class)
    public void test() {
        @NotNull final ProjectClient client = new ProjectClient();
        client.delete();
        Assert.assertTrue(client.get().isEmpty());
        @NotNull final Project project = new Project("test");
        client.post(project);
        Assert.assertNotNull(client.get());
        project.setName("test test");
        client.put(project);
        Assert.assertEquals("test test", client.get(project.getId()).getName());
        client.delete(project.getId());
        Assert.assertTrue(client.get().isEmpty());
        @NotNull final Project project2 = new Project("try");
        @NotNull List<Project> list = Arrays.asList(project, project2);
        client.post(list);
        Assert.assertEquals(2, client.get().size());
        project.setName("try test");
        project2.setName("test and try");
        list = Arrays.asList(project, project2);
        client.put(list);
        Assert.assertEquals("test and try", client.get(project2.getId()).getName());
        Assert.assertEquals("try test", client.get(project.getId()).getName());
    }

    @Test
    @Category(ApiCategory.class)
    public void testFeign() {
        ProjectClientFeign.client(PROJECTS).delete();
        Assert.assertTrue(ProjectClientFeign.client(PROJECTS).get().isEmpty());
        @NotNull final Project project = new Project("test");
        ProjectClientFeign.client(PROJECT).post(project);
        Assert.assertNotNull(ProjectClientFeign.client(PROJECTS).get());
        project.setName("test test");
        ProjectClientFeign.client(PROJECT).put(project);
        Assert.assertEquals("test test", ProjectClientFeign.client(PROJECT).get(project.getId()).getName());
        ProjectClientFeign.client(PROJECT).delete(project.getId());
        Assert.assertTrue(ProjectClientFeign.client(PROJECTS).get().isEmpty());
        @NotNull final Project project2 = new Project("try");
        @NotNull List<Project> list = Arrays.asList(project, project2);
        ProjectClientFeign.client(PROJECTS).post(list);
        Assert.assertEquals(2, ProjectClientFeign.client(PROJECTS).get().size());
        project.setName("try test");
        project2.setName("test and try");
        list = Arrays.asList(project, project2);
        ProjectClientFeign.client(PROJECTS).put(list);
        Assert.assertEquals("test and try", ProjectClientFeign.client(PROJECT).get(project2.getId()).getName());
        Assert.assertEquals("try test", ProjectClientFeign.client(PROJECT).get(project.getId()).getName());
    }

}
