import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.client.TaskClient;
import ru.vmaksimenkov.tm.client.TaskClientFeign;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.*;

public class TaskTest {

    @Test
    @Category(ApiCategory.class)
    public void test() {
        @NotNull final TaskClient client = new TaskClient();
        client.delete();
        Assert.assertTrue(client.get().isEmpty());
        @NotNull final Task task = new Task("test");
        client.post(task);
        Assert.assertNotNull(client.get());
        task.setName("test test");
        client.put(task);
        Assert.assertEquals("test test", client.get(task.getId()).getName());
        client.delete(task.getId());
        Assert.assertTrue(client.get().isEmpty());
        @NotNull final Task task2 = new Task("try");
        @NotNull List<Task> list = Arrays.asList(task, task2);
        client.post(list);
        Assert.assertEquals(2, client.get().size());
        task.setName("try test");
        task2.setName("test and try");
        list = Arrays.asList(task, task2);
        client.put(list);
        Assert.assertEquals("test and try", client.get(task2.getId()).getName());
        Assert.assertEquals("try test", client.get(task.getId()).getName());
    }

    @Test
    @Category(ApiCategory.class)
    public void testFeign() {
        TaskClientFeign.client(TASKS).delete();
        Assert.assertTrue(TaskClientFeign.client(TASKS).get().isEmpty());
        @NotNull final Task project = new Task("test");
        TaskClientFeign.client(TASK).post(project);
        Assert.assertNotNull(TaskClientFeign.client(TASKS).get());
        project.setName("test test");
        TaskClientFeign.client(TASK).put(project);
        Assert.assertEquals("test test", TaskClientFeign.client(TASK).get(project.getId()).getName());
        TaskClientFeign.client(TASK).delete(project.getId());
        Assert.assertTrue(TaskClientFeign.client(TASKS).get().isEmpty());
        @NotNull final Task project2 = new Task("try");
        @NotNull List<Task> list = Arrays.asList(project, project2);
        TaskClientFeign.client(TASKS).post(list);
        Assert.assertEquals(2, TaskClientFeign.client(TASKS).get().size());
        project.setName("try test");
        project2.setName("test and try");
        list = Arrays.asList(project, project2);
        TaskClientFeign.client(TASKS).put(list);
        Assert.assertEquals("test and try", TaskClientFeign.client(TASK).get(project2.getId()).getName());
        Assert.assertEquals("try test", TaskClientFeign.client(TASK).get(project.getId()).getName());
    }

}
